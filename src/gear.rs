// Copyright © 2018 Andrew Rogers
// [This program is licensed under the "MIT License"]
// Please see the file LICENSE-MIT in the source
// distribution of this software for license terms.

#[derive(Debug)]
enum Range {
	Melee,
	Close,
	Medium,
	Long,
}

#[derive(Debug)]
struct Weapon {
	damage: u16,
	range: Range,
	hardpoints: u8,
	encumb: u16,
	mods: Vec<String>,
}

#[derive(Debug)]
struct Equipment {
	name: String,
	encumb: u16,
	mods: Vec<String>,
}