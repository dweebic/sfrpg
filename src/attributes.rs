// Copyright © 2018 Andrew Rogers
// [This program is licensed under the "MIT License"]
// Please see the file LICENSE-MIT in the source
// distribution of this software for license terms.


// Not currently utilized as there isn't any mechanical aspect
// needed and will take up some screen space
#[derive(Debug)]
struct Background {
	description: String,
}

// Not currently utilized as this will require another window
// And a lot of screen space
#[derive(Debug)]
struct Obligation {
	ob_type: String,
	amount: u16,
	description: String,
}

#[derive(Debug, Clone)]
pub enum Ability {
	Brawn,
	Agility,
	Intellect,
	Cunning,
	Willpower,
	Presence,
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum Species {
	Bothan,
	Droid,
	Gand,
	Human,
	Rodian,
	Trandoshan,
	Twilek,
	Wookie,
}

impl Species {
	pub fn to_str(&self) -> &'static str {
		match self {
			Species::Bothan => "Bothan",
			Species::Droid => "Droid",
			Species::Gand => "Gand",
			Species::Human => "Human",
			Species::Rodian => "Rodian",
			Species::Trandoshan => "Trandoshan",
			Species::Twilek => "Twi'lek",
			Species::Wookie => "Wookie",
		}
	}

	pub fn from_combo(combobox: i64) -> Option<Species>  {
		match combobox {
			0 => Some(Species::Bothan),
			1 => Some(Species::Droid),
			2 => Some(Species::Gand),
			3 => Some(Species::Human),
			4 => Some(Species::Rodian),
			5 => Some(Species::Trandoshan),
			6 => Some(Species::Twilek),
			7 => Some(Species::Wookie),
			_ => None,			
		}
	}

	//derives wound threshold calculation
	pub fn init_wound_thresh(&self, brawn: u8) -> u8{
		match self {
			Species::Bothan => {
				10 + brawn
			},
			Species::Droid => {
				10 + brawn
			},
			Species::Gand => {
				10 + brawn
			},
			Species::Human => {
				10 + brawn
			},
			Species::Rodian => {
				10 + brawn
			},
			Species::Trandoshan => {
				12 + brawn
			},
			Species::Twilek => {
				10 + brawn
			},
			Species::Wookie => {
				14 + brawn
			},
		}
	}

		//derives strain threshold calculation
	pub fn init_strain_thresh(&self, willpower: u8) -> u8{
		match self {
			Species::Bothan => {
				11 + willpower
			},
			Species::Droid => {
				10 + willpower
			},
			Species::Gand => {
				10 + willpower
			},
			Species::Human => {
				10 + willpower
			},
			Species::Rodian => {
				10 + willpower
			},
			Species::Trandoshan => {
				9 + willpower
			},
			Species::Twilek => {
				11 + willpower
			},
			Species::Wookie => {
				8 + willpower
			},
		}
	}

		//derives strain threshold calculation
	pub fn init_xp(&self) -> u16{
		match self {
			Species::Bothan => {
				100_u16
			},
			Species::Droid => {
				175_u16
			},
			Species::Gand => {
				100_u16
			},
			Species::Human => {
				110_u16
			},
			Species::Rodian => {
				100_u16
			},
			Species::Trandoshan => {
				90_u16
			},
			Species::Twilek => {
				100_u16
			},
			Species::Wookie => {
				90_u16
			},
		}
	}

}

