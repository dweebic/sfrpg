// Copyright © 2018 Andrew Rogers
// [This program is licensed under the "MIT License"]
// Please see the file LICENSE-MIT in the source
// distribution of this software for license terms.


// Citation current code based on work from Leo Tindell 
// <https://crates.io/crates/iui>
// merged code from a couple pieces to mix UI and file saving
#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;

mod attributes;
mod character;
mod skills;

use attributes::*;
use character::*;
use skills::*;



// Demonstrates a mutable application state manipulated over a number of UIs merged
// With an ability to save

extern crate iui;
use iui::prelude::*;
use iui::controls::{Label, Button, Combobox, Spinbox, Slider, Entry, VerticalBox, HorizontalBox, HorizontalSeparator, Group};
use iui::menus::Menu;
use std::rc::Rc;
use std::cell::RefCell;
use std::error::Error;
use std::fs::File;
use std::io::Write;
use std::io::Read;




/// This struct will hold the values that multiple callbacks will need to access.
struct State {
    player: Character,
}

fn ui_manager() {

    //Initialization Character
    let test_bill = Character::new("Bill", "Andrew", 2, 1, 1, 1, 1, 1,
        Species::Human, "Technician", "Mechanic");    

    // Initialize the UI framework.
    let ui = UI::init().unwrap();

    let char_menu = Menu::new(&ui, "Characters");
    let char_open = char_menu.append_item("Open Character");
    let char_save = char_menu.append_item("Save Character");
    let new_char = char_menu.append_item("Make a New Character");

    let talent_menu = Menu::new(&ui, "Talents");
    let new_talent = talent_menu.append_item("Add a Talent");

    let session_menu = Menu::new(&ui, "Game Session");
    let award_xp = session_menu.append_item("Receive End of Session XP");

    // Initialize the state of the application.
    let state = Rc::new(RefCell::new(State {  player: test_bill.clone() }));


    // -----Beginning  Main window formatting ---------- --------


    // This group is the main alignment structure
    // This vertical box will arrange the character sheet elements.
    let mut main_vbox = VerticalBox::new(&ui);

        // The top block of the sheet
        let mut admin_hbox = HorizontalBox::new(&ui);
            // The Administrative, text heavy section at the top of the sheet
            let (character_group, player_label, character_label, species_label,
             career_label, specializations_label, talent_label) = {
                let mut character_group = Group::new(&ui, "Character Info" );
                let mut character_vbox = VerticalBox::new(&ui);

                let player_label = Label::new(&ui, "");
                let character_label = Label::new(&ui, "");
                let species_label = Label::new(&ui,"");
                let career_label = Label::new(&ui, "");
                let specializations_label = Label::new(&ui, "");
                let talent_label = Label::new(&ui, "");

                character_vbox.append(&ui, player_label.clone(), LayoutStrategy::Compact);
                character_vbox.append(&ui, character_label.clone(), LayoutStrategy::Compact);
                character_vbox.append(&ui, species_label.clone(), LayoutStrategy::Compact);
                character_vbox.append(&ui, career_label.clone(), LayoutStrategy::Compact);
                character_vbox.append(&ui, specializations_label.clone(), LayoutStrategy::Compact);
                character_vbox.append(&ui, talent_label.clone(), LayoutStrategy::Compact);
                character_group.set_child(&ui, character_vbox);
                (character_group, player_label, character_label, species_label,
                 career_label, specializations_label, talent_label)  
            };

            // Contains the Experience, Money, and Encumbrance Values
            let (accounting_group, curr_xp_label, tot_xp_label,
                 credits_label, encumbrance_label) = {
                let mut accounting_group = Group::new(&ui, "Accounting");
                let mut accounting_vbox = VerticalBox::new(&ui);

                let curr_xp_label = Label::new(&ui, "");
                let tot_xp_label = Label::new(&ui, "");
                let credits_label = Label::new(&ui, "");
                let encumbrance_label = Label::new(&ui, "");

                accounting_vbox.append(&ui, curr_xp_label.clone(), LayoutStrategy::Compact);
                accounting_vbox.append(&ui, tot_xp_label.clone(), LayoutStrategy::Compact);
                accounting_vbox.append(&ui, credits_label.clone(), LayoutStrategy::Compact);
                accounting_vbox.append(&ui, encumbrance_label.clone(), LayoutStrategy::Compact);
                accounting_group.set_child(&ui, accounting_vbox);
                (accounting_group, curr_xp_label, tot_xp_label, credits_label, encumbrance_label)
            };

            // Entry tools to update credits and XP
            let (entry_update_group, xp_title, mut set_xp_entry,
                    credits_title, mut credit_entry) = {
                let mut entry_update_group = Group::new(&ui, "Accounting Update");
                let mut entry_update_vbox = VerticalBox::new(&ui);

                let xp_title = Label::new(&ui, "Current XP");
                let mut set_xp_entry = Spinbox::new(&ui, 0, 500);
                set_xp_entry.set_value(&ui, state.borrow_mut().player.curr_xp() as i64);
                let credits_title = Label::new(&ui, "Current Credits");
                let mut credit_entry = Spinbox::new(&ui,0, 100_000_000_000);
                credit_entry.set_value(&ui, state.borrow_mut().player.total_credits() as i64);

                entry_update_vbox.append(&ui, xp_title.clone(), LayoutStrategy::Stretchy);
                entry_update_vbox.append(&ui, set_xp_entry.clone(), LayoutStrategy::Stretchy);
                entry_update_vbox.append(&ui, credits_title.clone(), LayoutStrategy::Stretchy);
                entry_update_vbox.append(&ui, credit_entry.clone(), LayoutStrategy::Stretchy);
                entry_update_group.set_child(&ui, entry_update_vbox);
                (entry_update_group, xp_title, set_xp_entry, credits_title, credit_entry)
            };

        admin_hbox.append(&ui, character_group.clone(), LayoutStrategy::Compact);
        admin_hbox.append(&ui, accounting_group.clone(), LayoutStrategy::Stretchy);
        admin_hbox.append(&ui, entry_update_group.clone(), LayoutStrategy::Stretchy);

    //-------------------------------Combatbox Middle Section

        let mut combat_hbox = HorizontalBox::new(&ui);

            //  Mostly derived attributes used during combat situations
            let (defense_group, soak_label, defense_label) = {
                let mut defense_group = Group::new(&ui, "Defense Values");
                let mut defense_vbox = VerticalBox::new(&ui);

                let soak_label      = Label::new(&ui, "");
                let defense_label   = Label::new(&ui, "");

                defense_vbox.append(&ui, soak_label.clone(), LayoutStrategy::Stretchy);
                defense_vbox.append(&ui, defense_label.clone(), LayoutStrategy::Stretchy);
                defense_group.set_child(&ui, defense_vbox);
                (defense_group, soak_label, defense_label)        
            };

            //  Sliders to manipulate the current wound and strain levels
            let (damage_group, wound_title, mut wound_slider, strain_title, mut strain_slider) = {
                let mut damage_group = Group::new(&ui, "Damage Adjustments");
                let mut damage_vbox = VerticalBox::new(&ui);
                damage_vbox.set_padded(&ui, true);

                let wound_title = Label::new(&ui, "Wounds");
                let mut wound_slider = Slider::new(&ui, 0, state.borrow_mut().player.wound_thresh() as i64);
                wound_slider.set_value(&ui, state.borrow_mut().player.curr_wound() as i64);
                let strain_title = Label::new(&ui, "Strain");
                let mut strain_slider = Slider::new(&ui, 0, state.borrow_mut().player.strain_thresh() as i64);
                strain_slider.set_value(&ui, state.borrow_mut().player.curr_strain() as i64);  

                damage_vbox.append(&ui, wound_title.clone(), LayoutStrategy::Stretchy);
                damage_vbox.append(&ui, wound_slider.clone(), LayoutStrategy::Compact);
                damage_vbox.append(&ui, strain_title.clone(), LayoutStrategy::Stretchy);
                damage_vbox.append(&ui, strain_slider.clone(), LayoutStrategy::Compact); 

                damage_group.set_child(&ui, damage_vbox);
                (damage_group, wound_title, wound_slider, strain_title, strain_slider)   
            };

        combat_hbox.append(&ui, defense_group.clone(), LayoutStrategy::Compact);
        combat_hbox.append(&ui, damage_group.clone(), LayoutStrategy::Stretchy);



        // Characteristics, sometimes called ability score that are used to derive other
        // values. They can only be changed at the beginning or with advanced talents
        // or cybernetic enhancedments
        let (ability_group, brawn_label, agility_label, intellect_label,
            cunning_label, willpower_label, presence_label) = {
            let mut ability_group = Group::new(&ui, "Characteristics");
            let mut ability_hbox = HorizontalBox::new(&ui);

            let brawn_label     = Label::new(&ui, "");
            let agility_label   = Label::new(&ui, "");
            let intellect_label = Label::new(&ui, "");
            let cunning_label   = Label::new(&ui, "");
            let willpower_label = Label::new(&ui, "");
            let presence_label  = Label::new(&ui, "");

            ability_hbox.append(&ui, brawn_label.clone(), LayoutStrategy::Stretchy);
            ability_hbox.append(&ui, agility_label.clone(), LayoutStrategy::Stretchy);
            ability_hbox.append(&ui, intellect_label.clone(), LayoutStrategy::Stretchy);
            ability_hbox.append(&ui, cunning_label.clone(), LayoutStrategy::Stretchy);
            ability_hbox.append(&ui, willpower_label.clone(), LayoutStrategy::Stretchy);
            ability_hbox.append(&ui, presence_label.clone(), LayoutStrategy::Stretchy);
            ability_group.set_child(&ui, ability_hbox);
            (ability_group, brawn_label, agility_label, intellect_label, cunning_label, willpower_label, presence_label)
        };

        // The key part of the sheet. Dice pools are based on ability scores and 
        // XP spent on ranking up skills. The UI doesn't have a way to rank up a skill
        // at this time.
        let (skill_group, skill_label_gen, skill_label_combat, skill_label_knowledge ) = {
            let mut skill_group = Group::new(&ui, "Skills");
            let mut skill_hbox = HorizontalBox::new(&ui);

            // Very upset with this. Need to figure out how to make this a 
            // slice or array. Currently all the skills are joined together by type
            let skill_label_gen = Label::new(&ui, "");
            let skill_label_combat = Label::new(&ui, "");
            let skill_label_knowledge = Label::new(&ui, "");

            skill_hbox.append(&ui, skill_label_gen.clone(), LayoutStrategy::Compact);
            skill_hbox.append(&ui, skill_label_combat.clone(), LayoutStrategy::Compact);
            skill_hbox.append(&ui, skill_label_knowledge.clone(), LayoutStrategy::Compact); 
            skill_group.set_child(&ui, skill_hbox);
            (skill_group, skill_label_gen, skill_label_combat, skill_label_knowledge )
        };


    //--- The framework for the UI as a whole
    main_vbox.append(&ui, admin_hbox, LayoutStrategy::Compact);
    main_vbox.append(&ui, HorizontalSeparator::new(&ui), LayoutStrategy::Compact);
    main_vbox.append(&ui, combat_hbox, LayoutStrategy::Compact);
    main_vbox.append(&ui, HorizontalSeparator::new(&ui), LayoutStrategy::Compact);
    main_vbox.append(&ui, ability_group, LayoutStrategy::Compact);
    main_vbox.append(&ui, HorizontalSeparator::new(&ui), LayoutStrategy::Compact);
    main_vbox.append(&ui, skill_group, LayoutStrategy::Compact);





    // The window allows all constituent components to be displayed.
    let mut window = Window::new(&ui, "Sci-Fi RPG Character Sheet", 1920, 1080, WindowType::HasMenubar);
    window.set_child(&ui, main_vbox);
    window.show(&ui);
// ---------------------------------End Main Window Formatting

// ------------------------Begin window controls and menu commands

    // These on_changed functions allow updating the application state when a
    // control changes its value.

    //Allows for dynamic updating of available Experience Points
    //during play buy altering the current state
    set_xp_entry.on_changed(&ui, {
        let state = state.clone();
        move |val| {state.borrow_mut().player.set_curr_xp(val)}
    });

    //Allows for dynamic updating of available credits, or currency on hand
    //during play buy altering the current state
    credit_entry.on_changed(&ui, {
        let state = state.clone();
        move |val| {state.borrow_mut().player.set_credits(val)}
    });

    //Allows for dynamic updating of wounds received or healed
    //during play buy altering the current state
    wound_slider.on_changed(&ui, {
        let state = state.clone();
        move |val| {state.borrow_mut().player.set_wound(val);}
    });

    //Allows for dynamic updating of strained received or removed
    //during play buy altering the current state
    strain_slider.on_changed(&ui, {
        let state = state.clone();
        move |val| {state.borrow_mut().player.set_strain(val);}
    });

    // This ensures that you cannot set the current XP higher
    // than the total XP amount
    set_xp_entry.on_changed(&ui, {
        let state = state.clone();
        
        move |val| {
            let total = state.borrow().player.total_xp() as i64;
            if val > total {
                state.borrow_mut().player.set_curr_xp(total);
            } else {

                state.borrow_mut().player.set_curr_xp(val);
            }
        }
    });

    // These on_clicked functions handle file control via the menu bar
    // selections via conversions to and from Character and serde JSON
    // Citation <https://serde.rs/derive.html>

    // Saves the current state of the company to a JSON file
    char_save.on_clicked(&ui, {
        let ui = ui.clone();
        let state = state.clone();
        
        move |_save, ref window| {
            if let Some(path) = window.save_file(&ui) {
                let mut file = match File::create(&path) {
                    Err(why) => { window.modal_err(&ui, "I/O Error", &format!("Could not open file {}: {}", path.display(), why.description())); return; }
                    Ok(f) => f
                };
                match file.write_all(state.borrow().player.json().as_bytes()) {
                    Err(why) => { window.modal_err(&ui, "I/O Error", &format!("Could not write to file {}: {}", path.display(), why.description())); return; }
                    Ok(_) => ()
                };
            }    
        }
    });

    // Opens a JSON file and modifies the current state to match the new file
    char_open.on_clicked(&ui, {
        let ui = ui.clone();
        let state = state.clone();
        let mut new_character_input = String::new();
        move |_open, ref window| {
            if let Some(path) = window.open_file(&ui) {
                let mut file = match File::open(&path) {
                    Err(why) => { window.modal_err(&ui, "I/O Error", &format!("Could not open file {}: {}", path.display(), why.description())); return; }
                    Ok(f) => f
                };

                match file.read_to_string(&mut new_character_input) {
                    Err(why) => { window.modal_err(&ui, "I/O Error", &format!("Could not write to file {}: {}", path.display(), why.description())); return; }
                    Ok(_) => ()
                };
            }    
        let new_character: Character = serde_json::from_str(&new_character_input).unwrap();
        state.borrow_mut().player = new_character;
        }
    });


    //-----------------New Character Window Start --------------------

    // Activate new character creation window when selected from menu
    new_char.on_clicked(&ui, {
        let ui = ui.clone();
        let state = state.clone();
        move |_open,  window| {
            //---------------------character window----------------
            // The Character Creator Window
            let mut char_window = Window::new(&ui, "New Character", 800, 800, WindowType::NoMenubar);
            let mut current_species = Species::Bothan;
            let (creator_group, new_name_label, new_name_entry, new_player_label,
                new_player_entry, new_career_label, new_career_entry, new_spec_label,
                new_spec_entry, new_species_label, mut species_combo, new_brawn_label,
                brawn_spin, new_agility_label, agility_spin,
                new_intellect_label, intellect_spin, new_cunning_label,
                cunning_spin, new_willpower_label, willpower_spin,
                new_presence_label, presence_spin, mut create_button) = {

                let mut creator_group = Group::new(&ui, "Enter New Character" );
                let mut creator_vbox = VerticalBox::new(&ui);

                let new_name_label = Label::new(&ui, "Character Name");
                let new_name_entry = Entry::new(&ui);
                let new_player_label = Label::new(&ui, "Player Name");
                let new_player_entry = Entry::new(&ui);
                let new_career_label = Label::new(&ui, "Career");
                let new_career_entry = Entry::new(&ui);
                let new_spec_label = Label::new(&ui, "Career");
                let new_spec_entry = Entry::new(&ui);
                let new_species_label = Label::new(&ui,"Species");
                let mut species_combo = Combobox::new(&ui);
                    species_combo.append(&ui, "Bothan");
                    species_combo.append(&ui, "Gand");
                    species_combo.append(&ui, "Human");
                    species_combo.append(&ui, "Rodian");
                    species_combo.append(&ui, "Trandoshan");
                    species_combo.append(&ui, "Twi'lek");
                    species_combo.append(&ui, "Wookie");
                    species_combo.set_selected(&ui, 0_i64);


                let new_brawn_label = Label::new(&ui, "Brawn");
                let brawn_spin = Spinbox::new(&ui, 1, 6);
                let new_agility_label = Label::new(&ui, "Agility");
                let agility_spin = Spinbox::new(&ui, 1, 6);
                let new_intellect_label = Label::new(&ui, "Intellect");
                let intellect_spin = Spinbox::new(&ui, 1, 6);
                let new_cunning_label = Label::new(&ui, "Cunning");
                let cunning_spin = Spinbox::new(&ui, 1, 6);
                let new_willpower_label = Label::new(&ui, "Willpower");
                let willpower_spin = Spinbox::new(&ui, 1, 6);
                let new_presence_label = Label::new(&ui, "Presence");
                let presence_spin = Spinbox::new(&ui, 1, 6);
                let mut create_button = Button::new(&ui, "Create");


                creator_vbox.append(&ui, new_name_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_name_entry.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_player_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_player_entry.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_career_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_career_entry.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_spec_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_spec_entry.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_species_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, species_combo.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_brawn_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, brawn_spin.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_agility_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, agility_spin.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_intellect_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, intellect_spin.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_cunning_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, cunning_spin.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_willpower_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, willpower_spin.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, new_presence_label.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, presence_spin.clone(), LayoutStrategy::Compact);
                creator_vbox.append(&ui, create_button.clone(), LayoutStrategy::Compact);
                creator_group.set_child(&ui, creator_vbox);
                (creator_group, new_name_label, new_name_entry, new_player_label,
                new_player_entry, new_career_label, new_career_entry, new_spec_label,
                new_spec_entry, new_species_label, species_combo, new_brawn_label,
                brawn_spin, new_agility_label, agility_spin, new_intellect_label,
                intellect_spin, new_cunning_label, cunning_spin, new_willpower_label,
                willpower_spin, new_presence_label, presence_spin, create_button)  
            };

           
            char_window.set_child(&ui, creator_group);

            // -- Manages the current value of the ComboBox
            species_combo.on_selected(&ui, {
                let ui = ui.clone();
                    move|value| {
                        current_species = Species::from_combo(value).unwrap();
                    }
            });

            char_window.show(&ui);
        

            // create button updates the player state to match the
            // form in the window
            create_button.on_clicked(&ui, {
                let ui = ui.clone();
                let state = state.clone();
                let char_window = char_window.clone();
                move |_| {
                    let form_character = Character::new(
                        &new_name_entry.value(&ui), 
                        &new_player_entry.value(&ui),
                        brawn_spin.value(&ui) as u8,
                        agility_spin.value(&ui) as u8,
                        intellect_spin.value(&ui) as u8,
                        cunning_spin.value(&ui) as u8,
                        willpower_spin.value(&ui) as u8,
                        presence_spin.value(&ui) as u8,
                        current_species,
                        &new_career_entry.value(&ui),
                        &new_spec_entry.value(&ui));
                state.borrow_mut().player = form_character;

                }
            });

            // --- Allow the secondary window to close without
            // --- Shutting down the application  
            char_window.on_closing(&ui, {
                move|window|
                unsafe {window.destroy();}
            });

        }
    
    });
    //---------------------------End of New Character Window------------------

    //-----------------New Talent Window Start --------------------

    // Activate new character creation window when selected from menu
    new_talent.on_clicked(&ui, {
        let ui = ui.clone();
        let state = state.clone();
        move |_open,  window| {
            //---------------------Talent window----------------
            // The Talent Creator Window
            let mut talent_window = Window::new(&ui, "New Talent", 800, 800, WindowType::NoMenubar);
            let (talent_group, new_talent_label, new_talent_entry, xp_cost_label,
                xp_cost_spin, mut talent_button) = {

                let mut talent_group = Group::new(&ui, "Enter New Talent" );
                let mut talent_vbox = VerticalBox::new(&ui);

                let new_talent_label = Label::new(&ui, "Talent Name");
                let new_talent_entry = Entry::new(&ui);
                let xp_cost_label = Label::new(&ui, "Talent's XP Cost");
                let xp_cost_spin = Spinbox::new(&ui, 1, 50);
                let mut talent_button = Button::new(&ui, "Add Talent");


                talent_vbox.append(&ui, new_talent_label.clone(), LayoutStrategy::Compact);
                talent_vbox.append(&ui, new_talent_entry.clone(), LayoutStrategy::Compact);
                talent_vbox.append(&ui, xp_cost_label.clone(), LayoutStrategy::Compact);
                talent_vbox.append(&ui, xp_cost_spin.clone(), LayoutStrategy::Compact);
                talent_vbox.append(&ui, talent_button.clone(), LayoutStrategy::Compact);
                talent_group.set_child(&ui, talent_vbox);
                (talent_group, new_talent_label, new_talent_entry, xp_cost_label,
                xp_cost_spin, talent_button)  
            };

           
            talent_window.set_child(&ui, talent_group);

            talent_window.show(&ui);
        

            //------------talent button to push state change
            talent_button.on_clicked(&ui, {
                let ui = ui.clone();
                let state = state.clone();
                let talent_window = talent_window.clone();
                move |_| {
                 state.borrow_mut().player.add_talent(&new_talent_entry.value(&ui),
                    xp_cost_spin.value(&ui) as u16);
                }
            });

            // --- Allow the secondary window to close without
            // --- Shutting down the application  
            talent_window.on_closing(&ui, {
                move|window|
                unsafe {window.destroy();}
            });

        }
    
    });
    //---------------------------End of New Talent Window------------------

    //-----------------Award End of Session XP Window Start --------------------

    // Activate new character creation window when selected from menu
    award_xp.on_clicked(&ui, {
        let ui = ui.clone();
        let state = state.clone();
        move |_open,  window| {
            //---------------------Talent window----------------
            // The Talent Creator Window
            let mut xp_window = Window::new(&ui, "Add End of Session Experience Points",
                800, 800, WindowType::NoMenubar);
            let (xp_group, new_xp_label, xp_award_spin, mut xp_button) = {

                let mut xp_group = Group::new(&ui, "Enter Received XP" );
                let mut xp_vbox = VerticalBox::new(&ui);

                let new_xp_label = Label::new(&ui, "Experience Points Awarded");
                let xp_award_spin = Spinbox::new(&ui, 1, 50);
                let mut xp_button = Button::new(&ui, "Update XP");


                xp_vbox.append(&ui, new_xp_label.clone(), LayoutStrategy::Compact);
                xp_vbox.append(&ui, xp_award_spin.clone(), LayoutStrategy::Compact);
                xp_vbox.append(&ui, xp_button.clone(), LayoutStrategy::Compact);
                xp_group.set_child(&ui, xp_vbox);
                (xp_group, new_xp_label, xp_award_spin, xp_button)  
            };

           
            xp_window.set_child(&ui, xp_group);

            xp_window.show(&ui);
        

            //------------talent button to push state change
            xp_button.on_clicked(&ui, {
                let ui = ui.clone();
                let state = state.clone();
                let xp_window = xp_window.clone();
                move |_| {
                 state.borrow_mut().player.award_xp(xp_award_spin.value(&ui) as u16);
                }
            });

            // --- Allow the secondary window to close without
            // --- Shutting down the application  
            xp_window.on_closing(&ui, {
                move|window|
                unsafe {window.destroy();}
            });

        }
    
    });
    //---------------------------End of Award End of Session XP Window------------------



    // The event loop is the real time updating of the values as the 
    // State reacts to changes in the manually changed or 
    // derived values inside a closure
    let mut event_loop = ui.event_loop();
    event_loop.on_tick(&ui, {
        let ui = ui.clone();

        let mut player_label = player_label.clone();
        let mut character_label = character_label.clone();
        let mut species_label = species_label.clone();
        let mut career_label = career_label.clone();
        let mut specializations_label = specializations_label.clone();
        let mut talent_label = talent_label.clone();

        let mut curr_xp_label = curr_xp_label.clone();
        let mut tot_xp_label = tot_xp_label.clone();
        let mut credits_label = credits_label.clone();
        let mut encumbrance_label = encumbrance_label.clone();

        let mut soak_label = soak_label.clone();
        let mut wound_title = wound_title.clone();
        let mut strain_title = strain_title.clone();
        let mut defense_label = defense_label.clone();

        let mut brawn_label = brawn_label.clone();
        let mut agility_label = agility_label.clone();
        let mut intellect_label = intellect_label.clone();
        let mut cunning_label = cunning_label.clone();
        let mut willpower_label = willpower_label.clone();
        let mut presence_label = presence_label.clone();

        let mut skill_label_gen = skill_label_gen.clone();
        let mut skill_label_combat = skill_label_combat.clone();
        let mut skill_label_knowledge = skill_label_knowledge.clone();

        move || {
            let state = state.borrow();

            // Update all the labels based on current state conditions

            player_label.set_text(&ui, &format!("Player:\t\t {}", state.player.player_name()));
            character_label.set_text(&ui, &format!("Character:\t {}", state.player.name()));
            species_label.set_text(&ui, &format!("Species:\t\t {}", state.player.species().to_str()));
            career_label.set_text(&ui, &format!("Career:\t\t {}", state.player.career()));
            specializations_label.set_text(&ui, &format!("Specializations:\t\t {}", state.player.joined_specialties()));
            talent_label.set_text(&ui, &format!("Talents:\t\t {}", state.player.joined_talents()));
            
            curr_xp_label.set_text(&ui, &format!("Current XP: {}", state.player.curr_xp()));
            tot_xp_label.set_text(&ui, &format!("Total XP: {}", state.player.total_xp()));
            set_xp_entry.set_value(&ui, state.player.curr_xp() as i64);
            credits_label.set_text(&ui, &format!("Credits: {}", state.player.total_credits()));
            credit_entry.set_value(&ui, state.player.total_credits() as i64);
            encumbrance_label.set_text(&ui, &format!("Encumbrance Threshold: {}", 
                state.player.encumb_thresh()));

            soak_label.set_text(&ui, &format!("Soak:\n {}", state.player.soak()));
            wound_title.set_text(&ui, &format!("Wounds:\t{}\t Threshold",
                state.player.wound_thresh()));
            wound_slider.set_value(&ui, state.player.curr_wound() as i64);
            strain_title.set_text(&ui, &format!("Strain:\t{}\t Threshold",
                state.player.strain_thresh()));
            strain_slider.set_value(&ui, state.player.curr_strain() as i64);
            defense_label.set_text(&ui, &format!("Defense:\n\t{}\t/\t{}\n Ranged / Melee",
                state.player.defense_ranged(), state.player.defense_melee()));

            
            brawn_label.set_text(&ui, &format!("Brawn:\n{}", state.player.brawn()));
            agility_label.set_text(&ui, &format!("Agility:\n{}", state.player.agility()));
            intellect_label.set_text(&ui, &format!("Intellect:\n{}", state.player.intellect()));
            cunning_label.set_text(&ui, &format!("Cunning:\n{}", state.player.cunning()));
            willpower_label.set_text(&ui, &format!("Willpower:\n{}", state.player.willpower()));
            presence_label.set_text(&ui, &format!("Presence:\n{}", state.player.presence()));

            let skill_text = {
                let mut gen_builder = "".to_string();
                let mut combat_builder = "".to_string();
                let mut knowledge_builder = "".to_string();
                for skill_segment in state.player.skill_set() {
                    let mut text_builder = "".to_string();
                    text_builder.push_str(&skill_segment.name());
                    text_builder.push_str(": ");
                    text_builder.push_str(&format!("Green Dice:\t{}\tYellow Dice:\t{}\t",
                        skill_segment.green_dice(), skill_segment.yellow_dice()));
                    if skill_segment.is_career_skill() {
                        text_builder.push_str("Career\t\n");
                    } else {
                        text_builder.push_str("\t\t\n");
                    }
                    match skill_segment.skill_type() {
                        SkillType::General => gen_builder.push_str(&text_builder),
                        SkillType::Combat  => combat_builder.push_str(&text_builder),
                        SkillType::Knowledge => knowledge_builder.push_str(&text_builder),
                    }
                }
                (gen_builder, combat_builder, knowledge_builder)
            };

            skill_label_gen.set_text(&ui, &skill_text.0);
            skill_label_combat.set_text(&ui, &skill_text.1);
            skill_label_knowledge.set_text(&ui, &skill_text.2);
        }
    });
    event_loop.run(&ui);
}

fn main() {
    ui_manager();
}