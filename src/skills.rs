// Copyright © 2018 Andrew Rogers
// [This program is licensed under the "MIT License"]
// Please see the file LICENSE-MIT in the source
// distribution of this software for license terms.


use attributes::{ Ability};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Skill {
	name: String,
	ability: u8,
	career_skill: bool, 
	skill_type: SkillType,
	rank: u8,
}

impl Skill {

	// basic constructor for the Skill type
	pub fn new(name: &str, ability: u8, career: bool, skill_type: SkillType) -> Skill {
		Skill{
			name: name.to_string(),
			ability: ability,
			career_skill: career,
			skill_type: skill_type,
			rank: 0_u8,
		}
	}

	// increments the rank of a Skill
	pub fn upgrade(self) -> Skill {
		let new_rank = self.rank + 1;
		Skill {
			name: self.name,
			ability: self.ability,
			career_skill: self.career_skill,
			skill_type: self.skill_type,
			rank: new_rank,
		}
	}

	pub fn name(&self) -> String {
		self.name.clone()
	}

	pub fn skill_type(&self) -> SkillType {
		self.skill_type.clone()
	}

	// getter needed for skill upgrade cost calculations
	pub fn is_career_skill(&self) -> bool {
		self.career_skill
	}

	// getter to determine which attribute is needed for skill calculations
	pub fn ability(&self) -> u8 {
		self.ability
	}

	// returns the rank for skill calculations
	pub fn rank(&self) -> u8 {
		self.rank
	}

	pub fn player_dice(&self) -> (u8, u8) {
		let total_dice = if self.ability >= self.rank {
			self.ability
		} else {
			self.rank
		};
		let upgrade_dice = if self.rank <= self.ability {
			self.rank
		} else {
			self.ability
		};
		(total_dice, upgrade_dice)
	}

	pub fn green_dice(&self) -> u8 {
		self.player_dice().0 - self.player_dice().1
	}

	pub fn yellow_dice(&self) -> u8 {
		self.player_dice().1
	}

}

#[derive(Serialize, Deserialize, Debug, Clone, Eq, PartialEq)]
pub enum SkillType {
	General,
	Combat,
	Knowledge,
}


pub struct SkillList<'a> {
	name: &'a str,
	ability: Ability,
	skill_type: SkillType,
}

impl SkillList<'static> {
	pub fn name(&self) -> &'static str {
		self.name
	}

	pub fn ability(&self) -> Ability {
		self.ability.clone()
	}

	pub fn skill_type(&self) -> SkillType {
		self.skill_type.clone()
	}
}

const ASTROGATION: SkillList<'static> = SkillList{
	name: "Astrogation",
	ability: Ability::Intellect,
	skill_type: SkillType::General,
};

const ATHLETICS: SkillList<'static> = SkillList{
	name: "Athletics",
	ability: Ability::Brawn,
	skill_type: SkillType::General,
};

const BRAWL: SkillList<'static> = SkillList{
	name: "Brawl",
	ability: Ability::Brawn,
	skill_type: SkillType::Combat,
};

const CHARM: SkillList<'static> = SkillList{
	name: "Charm",
	ability: Ability::Presence,
	skill_type: SkillType::General,
};

const COERCION: SkillList<'static> = SkillList{
	name: "Coercion",
	ability: Ability::Willpower,
	skill_type: SkillType::General,
};

const COMPUTERS: SkillList<'static> = SkillList{
	name: "Computers",
	ability: Ability::Intellect,
	skill_type: SkillType::General,
};

const COOL: SkillList<'static> = SkillList{
	name: "Cool",
	ability: Ability::Presence,
	skill_type: SkillType::General,
};

const COORDINATION: SkillList<'static> = SkillList{
	name: "Coordination",
	ability: Ability::Agility,
	skill_type: SkillType::General,
};

const CORE_WORLDS: SkillList<'static> = SkillList{
	name: "Core Worlds",
	ability: Ability::Intellect,
	skill_type: SkillType::Knowledge,
};

const DECEPTION: SkillList<'static> = SkillList{
	name: "Deception",
	ability: Ability::Cunning,
	skill_type: SkillType::General,
};

const DISCIPLINE: SkillList<'static> = SkillList{
	name: "Discipline",
	ability: Ability::Willpower,
	skill_type: SkillType::General,
};

const EDUCATION: SkillList<'static> = SkillList{
	name: "Education",
	ability: Ability::Intellect,
	skill_type: SkillType::Knowledge,
};

const GUNNERY: SkillList<'static> = SkillList{
	name: "Gunnery",
	ability: Ability::Agility,
	skill_type: SkillType::Combat,
};

const LEADERSHIP: SkillList<'static> = SkillList{
	name: "Leadership",
	ability: Ability::Presence,
	skill_type: SkillType::General,
};

const LORE: SkillList<'static> = SkillList{
	name: "Lore",
	ability: Ability::Intellect,
	skill_type: SkillType::Knowledge,
};

const MECHANICS: SkillList<'static> = SkillList{
	name: "Mechanics",
	ability: Ability::Intellect,
	skill_type: SkillType::General,
};

const MEDICINE: SkillList<'static> = SkillList{
	name: "Medicine",
	ability: Ability::Intellect,
	skill_type: SkillType::General,
};

const MELEE: SkillList<'static> = SkillList{
	name: "Melee",
	ability: Ability::Brawn,
	skill_type: SkillType::Combat,
};

const NEGOTIATION: SkillList<'static> = SkillList{
	name: "Negotiation",
	ability: Ability::Presence,
	skill_type: SkillType::General,
};

const OUTER_RIM: SkillList<'static> = SkillList{
	name: "Outer Rim",
	ability: Ability::Intellect,
	skill_type: SkillType::Knowledge,
};

const PERCEPTION: SkillList<'static> = SkillList{
	name: "Perception",
	ability: Ability::Cunning,
	skill_type: SkillType::General,
};

const PLANETARY: SkillList<'static> = SkillList{
	name: "Piloting-Planetary",
	ability: Ability::Agility,
	skill_type: SkillType::General,
};

const SPACE: SkillList<'static> = SkillList{
	name: "Piloting-Space",
	ability: Ability::Agility,
	skill_type: SkillType::General,
};

const RANGED_HEAVY: SkillList<'static> = SkillList{
	name: "Ranged-Heavy",
	ability: Ability::Agility,
	skill_type: SkillType::Combat,
};

const RANGED_LIGHT: SkillList<'static> = SkillList{
	name: "Ranged-Light",
	ability: Ability::Agility,
	skill_type: SkillType::Combat,
};

const RESILIENCE: SkillList<'static> = SkillList{
	name: "Resilience",
	ability: Ability::Brawn,
	skill_type: SkillType::General,
};

const SKULDUGGERY: SkillList<'static> = SkillList{
	name: "Skulduggery",
	ability: Ability::Cunning,
	skill_type: SkillType::General,
};

const STEALTH: SkillList<'static> = SkillList{
	name: "Stealth",
	ability: Ability::Agility,
	skill_type: SkillType::General,
};

const STREETWISE: SkillList<'static> = SkillList{
	name: "Streetwise",
	ability: Ability::Cunning,
	skill_type: SkillType::General,
};

const SURVIVAL: SkillList<'static> = SkillList{
	name: "Survival",
	ability: Ability::Cunning,
	skill_type: SkillType::General,
};

const UNDERWORLD: SkillList<'static> = SkillList{
	name: "Underworld",
	ability: Ability::Intellect,
	skill_type: SkillType::Knowledge,
};

const VIGILANCE: SkillList<'static> = SkillList{
	name: "Vigilance",
	ability: Ability::Willpower,
	skill_type: SkillType::General,
};

const XENOLOGY: SkillList<'static> = SkillList{
	name: "Xenology",
	ability: Ability::Intellect,
	skill_type: SkillType::Knowledge,
};

pub const SKILL_LIST: &[SkillList] = &[
	ASTROGATION,
	ATHLETICS,
	BRAWL,
	CHARM,
	COERCION,
	COMPUTERS,
	COOL,
	COORDINATION,
	CORE_WORLDS,
	DECEPTION,
	DISCIPLINE,
	EDUCATION,
	GUNNERY,
	LEADERSHIP,
	LORE,
	MECHANICS,
	MEDICINE,
	MELEE,
	NEGOTIATION,
	OUTER_RIM,
	PERCEPTION,
	PLANETARY,
	SPACE,
	RANGED_HEAVY,
	RANGED_LIGHT,
	RESILIENCE,
	SKULDUGGERY,
	STEALTH,
	STREETWISE,
	SURVIVAL,
	UNDERWORLD,
	VIGILANCE,
	XENOLOGY,
]; 

#[test]
fn test_upgrade() {
	let test_skill = Skill::new("juggling", 3, false, SkillType::General);
	assert_eq!(test_skill.rank, 0_u8);
	let test_skill = test_skill.upgrade();
	assert_eq!(test_skill.rank, 1_u8);
}

#[test]
fn test_green_dice() {
	let test_skill = Skill::new("juggling", 3, false, SkillType::General);
	assert_eq!(test_skill.rank, 0_u8);
	assert_eq!(test_skill.green_dice(), 3);
}

#[test]
fn test_yellow_dice_upgrade() {
	let test_skill = Skill::new("juggling", 3, false, SkillType::General);
	let test_skill = test_skill.upgrade();
	assert_eq!(test_skill.green_dice(), 2);
	assert_eq!(test_skill.yellow_dice(), 1);
}