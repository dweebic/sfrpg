// Copyright © 2018 Andrew Rogers
// [This program is licensed under the "MIT License"]
// Please see the file LICENSE-MIT in the source
// distribution of this software for license terms.


use attributes::*;
use skills::*;

use serde_json;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Character {
	char_name: String, //The name of the character being played
	player_name: String, //The owner of the character
	wound_thresh: u8,
	curr_wound: u8,
	strain_thresh: u8,
	curr_strain: u8,
	brawn: u8,
	agility: u8,
	intellect: u8,
	cunning: u8,
	willpower: u8,
	presence: u8,
	species: Species, // The race of the character
	career: String, // The fixed base career of the character
	specialty: Vec<String>, //A character can add more down the road.
	skill_set: Vec<Skill>,
	talent_set: Vec<String>,
	defense_melee: u8,
	defense_ranged: u8,
	soak: u8,
	total_xp: u16,
	curr_xp: u16,
	credits: u32,
	encumb_thresh: u8,
}



impl Character {


	pub fn new (char_name: &str, player_name: &str, brawn: u8,
		agility: u8, intellect: u8, cunning: u8, willpower: u8,
		presence: u8, species: Species , career: &str, specialty: &str) -> Self {


		// derived attributes
		let wound_thresh = species.init_wound_thresh(brawn);
		let strain_thresh = species.init_strain_thresh(willpower);
		let defense_melee = 0;
		let defense_range = 0;
		let soak = brawn;
		let total_xp = species.init_xp();
		let encumb_thresh = 5 + brawn;

		let mut skill_set = Vec::new();

		//Building Skills vector
		for char_skill in SKILL_LIST {
			let skill_ability = match char_skill.ability() {
				Ability::Brawn => brawn,
				Ability::Agility => agility,
				Ability::Intellect => intellect,
				Ability::Cunning => cunning,
				Ability::Willpower => willpower,
				Ability::Presence => presence,
			};
			let new_skill = Skill::new(
				char_skill.name(),
				skill_ability,
				false,
				char_skill.skill_type()
			);
			skill_set.push(new_skill);
		}

		Character {
			char_name: char_name.to_string(),
			player_name: player_name.to_string(),
			wound_thresh: wound_thresh,
			curr_wound: 0,
			strain_thresh: strain_thresh,
			curr_strain: 0,
			brawn: brawn,
			agility: agility,
			intellect: intellect,
			cunning: cunning,
			willpower: willpower,
			presence: presence,
			career: career.to_string(),
			specialty: vec!(specialty.to_string()),
			skill_set: skill_set.clone(),
			talent_set: Vec::new(),
			defense_melee: defense_melee,
			defense_ranged: defense_range,
			soak: soak,
			total_xp: total_xp,
			curr_xp: total_xp,
			credits: 0,
			encumb_thresh: encumb_thresh,
			species: species,			
		}

	}

	// getter for character's name
	pub fn name(&self) -> &str {
		&self.char_name
	}

	// getter for player's name
	pub fn player_name(&self) -> &str {
		&self.player_name
	}

	// getter for character Species
	pub fn species(&self) -> Species {
		self.species.clone()
	}

	// getter for career
	pub fn career(&self) -> &str {
		&self.career
	}


	// getter for only the original specialty
	pub fn prime_specialty(&self) -> &str {
		self.specialty.first().unwrap()
	}

	// getter for a Vec of all the specialties
	pub fn all_specialties(&self) -> Vec<String> {
		self.specialty.clone()
	}

	// getter for a single String of all the specialities
	pub fn joined_specialties(&self) -> String {
		let joined = self.specialty.iter()
			.fold(String::new(), |temp, ref i| {
				temp + &i + ", "
			});
		joined
	}

	// adds an additional specialty
	pub fn add_specialty(&mut self, specialty: &str) {
		&self.specialty.push(specialty.to_string());
	}

	// adds an additional talent
	pub fn add_talent(&mut self, talent: &str, cost: u16) {
		if cost <= self.curr_xp {
			self.curr_xp -= cost;
			&self.talent_set.push(talent.to_string());			
		}
	}

	// getter for a Vec of all the talents
	pub fn all_talents(&self) -> Vec<String> {
		self.talent_set.clone()
	}

	// getter for a single String containing all the talents
	pub fn joined_talents(&self) -> String {
		let joined = self.talent_set.iter()
			.fold(String::new(), |temp, ref i| {
				temp + &i + ", "
			});
		joined
	}

	// getter for total_xp
	pub fn total_xp(&self) -> u16 {
		self.total_xp
	}

	// increase total and currently available experience points after a session
	pub fn award_xp(&mut self, val: u16) {
		self.total_xp += val;
		self.curr_xp += val;
	}

	// getter for current available XP to spend
	pub fn curr_xp(&self) -> u16 {
		self.curr_xp
	}

	// setting for the current XP
	pub fn set_curr_xp(&mut self, val: i64) {
		self.curr_xp = val as u16;
	}

	// getter for the current available credits
	pub fn total_credits(&self) -> u32 {
		self.credits
	}

	// setter for the current available credits
	pub fn set_credits(&mut self, val: i64) {
		println!("{:?}", val);
		self.credits = val as u32;
	}

	// getter for the derived encumbrance threshold
	pub fn encumb_thresh(&self) -> u8 {
		self.encumb_thresh
	}

	// getter of the derived soak value
	pub fn soak(&self) -> u8 {
		self.soak
	}

	// getter of the derived wound threshold value
	pub fn wound_thresh(&self) -> u8 {
		self.wound_thresh
	}

	// getter of the current wounds that are tracked on a slider
	pub fn curr_wound(&self) -> u8 {
		self.curr_wound
	}

	// setter of the state's current wounds based off of the slider value
	pub fn set_wound(&mut self, val: i64) {
		self.curr_wound = val as u8;
	}

	// getter of the derived strain threshold value
	pub fn strain_thresh(&self) -> u8 {
		self.strain_thresh
	}

	// getter of the current strain that is tracked on a slider
	pub fn curr_strain(&self) -> u8 {
		self.curr_strain
	}

	// setter of the state's current strain based off of the slider value
	pub fn set_strain(&mut self, val: i64) {
		self.curr_strain = val as u8;
	}

	// getter of derived defense ability based user's equipment
	pub fn defense_ranged(&self) -> u8 {
		self.defense_ranged
	}

	// getter of derived defense ability based user's equipment
	pub fn defense_melee(&self) -> u8 {
		self.defense_melee
	}

	// getter of the brawn characteristic
	pub fn brawn(&self) -> u8 {
		self.brawn
	}

	// getter of the agility characteristic
	pub fn agility(&self) -> u8 {
		self.agility
	}

	// getter of the intellect characteristic
	pub fn intellect(&self) -> u8 {
		self.intellect
	}

	// getter of the cunning characteristic
	pub fn cunning(&self) -> u8 {
		self.cunning
	}

	// getter of the willpower characteristic
	pub fn willpower(&self) -> u8 {
		self.willpower
	}

	// getter of the presence characteristic
	pub fn presence(&self) -> u8 {
		self.presence
	}

	// getter of a Vec of all the character skills
	pub fn skill_set(&self) -> Vec<Skill> {
		self.skill_set.clone()
	}

	// generates a JSON string containing the complete Character Structure
	// for saving the character to a file.
	pub fn json(&self) -> String {
		serde_json::to_string(&self).unwrap()
	}
//TODO: add skill manipulation

}

#[test]
fn test_brawn() {
	let bill = Character::new("Bill", "Andrew", 2, 1, 1, 1, 1, 1,
		Species::Human, "Technician", "Mechanic");
	assert_eq!(bill.brawn, 2 );
}


#[test]
fn test_specialty() {
	let bill = Character::new("Bill", "Andrew", 2, 1, 1, 1, 1, 1,
		Species::Human, "Technician", "Mechanic");
	assert_eq!(bill.prime_specialty(), "Mechanic" );
	assert_eq!(bill.prime_specialty(), "Mechanic" );
}

#[test]
fn test_add_specialty() {
	let mut bill = Character::new("Bill", "Andrew", 2, 1, 1, 1, 1, 1,
		Species::Human, "Technician", "Mechanic");	
	bill.add_specialty("Outlaw Tech");
	assert_eq!(bill.prime_specialty(), "Mechanic" );
	let specialty_list = bill.all_specialties();
	assert_eq!(specialty_list[1], "Outlaw Tech" );

}

#[test]
fn test_init_xp() {
	let bill = Character::new("Bill", "Andrew", 2, 1, 1, 1, 1, 1,
		Species::Human, "Technician", "Mechanic");
	assert_eq!(bill.total_xp(), 110_u16 );
}


#[test]
fn test_add_talent() {
	let mut bill = Character::new("Bill", "Andrew", 2, 1, 1, 1, 1, 1,
		Species::Human, "Technician", "Mechanic");
	bill.add_talent("Gearhead", 5);
	assert_eq!(bill.curr_xp(), 105_u16 );
	assert!(bill.all_talents().contains(&"Gearhead".to_string()));
}