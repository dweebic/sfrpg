# Character Manager for Fantasy Flight Games Edge of the Empire

**Author: Andrew Rogers**

## License
Copyright © 2018 Andrew Rogers
[This program is licensed under the "MIT License"]
Please see the file LICENSE-MIT in the source
distribution of this software for license terms.


## Design Intent
The goal of this project was to make a tool to speed up referencing data needed on a character sheet for the Edge of the Empire Role Playing Game. It also performs a lot of the accounting needed as well as expediting skill checks through determining the right number of dice needed to add to dice pools based on your abilities and skill ranks.

## Lessons Learned
There were a few key topics that I wanted to explore during the development of this project.

+ **Developing a GUI:** I have never created a UI before, so I wanted a reason to make one.

+ **Understanding an unfamiliar crate:** Using an existing GUI crate of which I had no previous education, I was required to spend time searching the documentation and source code to understand the developer's intent. As the iui crate that I was using did not have very robust documentation or examples, I needed to spend a lot of time in the crate's source code to determine how to accomplish tasks that were not explicitly described in the documentation and examples.

+ **Macro usage:** After learning about macros in class, I realized that I could refactor the ugly code that describes the layout of the GUI. Unfortunately, the code base was mostly completed before I was able to practice using them. I intend to refactor main.rs file to use them for adding UI controls.

## Overview of Implementation 

This project is primarily built around Leo Tindall's iui UI crate. The serde-json crate is utilized for storing and retrieving the character data.

As of this update, management of the UI is regrettably maintained within the **main.rs** file. Further study and practice is needed to modularize the code and improve the graphic design and layout.

As for the implementation of the Character data structure and the automation of the game mechanics and references, the files **character.rs**, **skills.rs**, and **attributes.rs** contain the data structures and methods for the rules.

### attributes.rs
This is a very basic collection of enums to organize some of the most core components of Fantasy Flight's Edge of the Empire Role-playing Game.

The **Ability** enum organizes how and which of the ability scores are needed for different skill checks.

The **Species** enum allows for easier character creation by computing the different base attributes of the various species. Implementing expansion content will be performed by adding to this enum.

### skills.rs
The data structures and the functions and methods contained within this file were the impetus for the project. As opposed to other tabletop role-playing games, Edge of the Empire has an interesting way on how skill checks are performed. Since they use unique dice and variable numbers of dice depending upon a character's ability scores and proficiency in different skills, determining how many and what kind of dice are needed for various checks.

The methods and constants found within this file build the lower part of the character sheet and will really speed up play.

### character.rs

This is a massive data structure containing all of the character attributes as well as public getter and setter methods to access the structure. It also produces a JSON string to facilitate saving the character to an external file.

### Other Files

**maz.json** and **test.json** are two sample characters in json format that work with this application. The maz.json closely reflects a character that I actually have in a current Edge of the Empire campaign. One item of note is that since I cannot currently manipulate skill attributes within my program, I currently have to manipulate them in the json file itself.

The png files are screengrabs showing the current UI.

## Currently Implemented
+ The UI opens with a dummy character initialized, but I have now implemented a character creation window to be used to build a character from within the application.

![Main Window](maz_character.png)

+ Current Wounds and Current Strain can be manipulated via sliders. Their maximum values are gated by the characters max values.

+ Credits and Available XP to spend can be manipulated with a Spinbox

+ We can save the current character to a JSON file as well as load a character from a JSON file

+ A new talent menu item opens up a window to add a talent to the character if they have sufficient XP to spend and then deducts the appropriate amount of XP from their currently available amount.

+ Game Session Menu added and have implement a function to increase the total and current experience points in a new window

![The currently implemented secondary windows](Character_Menus.png)

## Roadmap of new items to Implement

+ Better modularization of the UI base code

+ Beautify and reorganize layout: expect continous improvement here

+ Adding XP accounting to updating and improving skills, etc

+ Skill updating

+ Adding Gear management and allow them to impact character traits

+ Adding secondary windows for background and obligation information

## Installation requirements:

This program was developed and testing on Ubuntu 16.04 LTS

The following requirements are called out by the iui crate to use the libui submodule
<https://crates.io/crates/iui> driving the UI:

>iui is the safe Rust wrapper, to be used by most users. ui-sys is the raw unsafe bindings to the libui C code. Requires cmake so it can build libui. libui is included as a submodule.

+ CMAKE

**For Linux installs**
+ GTK+-3.0 

Unclear of what is needed for Windows/Mac